﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC.Core.Models
{
    public class User
    {
        public string Name { get; set; }
        [Required][MaxLength(50)]
        public string Login { get; set; }
        [Required]
        public string Password { get; set; }
        public List<Challenge> Challenges { get; set; }

        public int Id { get; set; }
    }
}

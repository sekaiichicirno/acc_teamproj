﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ACC.Core.Models.DTO
{
    public class MoviesByTitleResponse
    {
        [JsonProperty("Search")]
        public List<Movie> Movies { get; set; }
        public bool Response { get; set; }
    }
}

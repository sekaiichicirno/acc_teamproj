﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC.Core.Models
{
    public class ChallengeItem
    {
        public Movie Movie { get; set; }
        public bool IsCompleted { get; set; }
        public int Score { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime? DateCompleted { get; set; }

        public Challenge Challenge { get; set; } //foreign key
        public int Id { get; set; }
    }
}

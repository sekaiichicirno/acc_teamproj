﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC.Core.Models
{
    public class Challenge
    {
        [Required][MaxLength(50)]
        public string Title { get; set; }
        public List<ChallengeItem> ChallengeItems { get; set; }
        public bool IsCompleted { get; set; }//завершен-не завершен
        public bool IsDropped { get; set; } //отложен-не отложен
        public int MaxItems { get; set; }

        public int Id { get; set; }
        public User User { get; set; } //foreign key
    }
}

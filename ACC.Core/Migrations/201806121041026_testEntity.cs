namespace ACC.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testEntity : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ChallengeItems", "Challenge_Id", "dbo.Challenges");
            DropIndex("dbo.ChallengeItems", new[] { "Challenge_Id" });
            AlterColumn("dbo.ChallengeItems", "Challenge_Id", c => c.Int());
            CreateIndex("dbo.ChallengeItems", "Challenge_Id");
            AddForeignKey("dbo.ChallengeItems", "Challenge_Id", "dbo.Challenges", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChallengeItems", "Challenge_Id", "dbo.Challenges");
            DropIndex("dbo.ChallengeItems", new[] { "Challenge_Id" });
            AlterColumn("dbo.ChallengeItems", "Challenge_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.ChallengeItems", "Challenge_Id");
            AddForeignKey("dbo.ChallengeItems", "Challenge_Id", "dbo.Challenges", "Id", cascadeDelete: true);
        }
    }
}

namespace ACC.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedFormat : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Challenges",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        IsCompleted = c.Boolean(nullable: false),
                        IsDropped = c.Boolean(nullable: false),
                        MaxItems = c.Int(nullable: false),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.ChallengeItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Movie_Title = c.String(),
                        Movie_Year = c.String(),
                        Movie_ImdbID = c.String(),
                        Movie_Poster = c.String(),
                        MovieId = c.String(),
                        IsCompleted = c.Boolean(nullable: false),
                        Score = c.Int(nullable: false),
                        DateAdded = c.DateTime(nullable: false),
                        DateCompleted = c.DateTime(),
                        Challenge_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Challenges", t => t.Challenge_Id)
                .Index(t => t.Challenge_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Login = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Challenges", "User_Id", "dbo.Users");
            DropForeignKey("dbo.ChallengeItems", "Challenge_Id", "dbo.Challenges");
            DropIndex("dbo.ChallengeItems", new[] { "Challenge_Id" });
            DropIndex("dbo.Challenges", new[] { "User_Id" });
            DropTable("dbo.Users");
            DropTable("dbo.ChallengeItems");
            DropTable("dbo.Challenges");
        }
    }
}

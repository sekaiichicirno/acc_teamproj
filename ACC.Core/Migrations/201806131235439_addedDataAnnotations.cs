namespace ACC.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedDataAnnotations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Challenges", "Title", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Users", "Login", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Users", "Password", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Password", c => c.String());
            AlterColumn("dbo.Users", "Login", c => c.String());
            AlterColumn("dbo.Challenges", "Title", c => c.String());
        }
    }
}

namespace ACC.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dataTest : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ChallengeItems", "MovieId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ChallengeItems", "MovieId", c => c.String());
        }
    }
}

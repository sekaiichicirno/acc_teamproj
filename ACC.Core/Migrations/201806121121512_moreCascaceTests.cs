namespace ACC.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class moreCascaceTests : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Challenges", "User_Id", "dbo.Users");
            DropIndex("dbo.Challenges", new[] { "User_Id" });
            AlterColumn("dbo.Challenges", "User_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Challenges", "User_Id");
            AddForeignKey("dbo.Challenges", "User_Id", "dbo.Users", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Challenges", "User_Id", "dbo.Users");
            DropIndex("dbo.Challenges", new[] { "User_Id" });
            AlterColumn("dbo.Challenges", "User_Id", c => c.Int());
            CreateIndex("dbo.Challenges", "User_Id");
            AddForeignKey("dbo.Challenges", "User_Id", "dbo.Users", "Id");
        }
    }
}

namespace ACC.Core.Migrations
{
    using ACC.Core.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ACC.Core.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ACC.Core.Context context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            Init(context);
            context.SaveChanges();
        }

        private void Init(Context context)
        {
            var challengeItem = new ChallengeItem();
            challengeItem.Score = 10;
            challengeItem.IsCompleted = true;
            challengeItem.DateAdded = DateTime.ParseExact("10/10/2012", "dd/MM/yyyy",null);
            challengeItem.DateCompleted = DateTime.ParseExact("12/12/2012", "dd/MM/yyyy", null);
            //challengeItem.MovieId = "tt0126029";
            challengeItem.Movie = new Movie
            {
                Title = "Shrek",
                Year = "2001",
                ImdbID = "tt0126029",
                Poster = "https://ia.media-imdb.com/images/M/MV5BOGZhM2FhNTItODAzNi00YjA0LWEyN2UtNjJlYWQzYzU1MDg5L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg"
            };
            var user = new User
            {
                Name = "admin",
                Login = "admin",
                Password = "ISMvKXpXpadDiUoOSoAfww==",
                Challenges = new List<Challenge>
                {
                    new Challenge
                    {
                        ChallengeItems = new List<ChallengeItem>{challengeItem},
                        IsCompleted=true,
                        MaxItems=1,
                        Title="basic",
                        IsDropped=false
                    }
                }
            };
            context.Users.AddOrUpdate(u=>u.Login,user);
        }
    }
}

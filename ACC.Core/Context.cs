﻿using ACC.Core.Models;
using System.Data.Entity;

namespace ACC.Core
{
    public class Context : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Challenge> Challenges { get; set; }
        //public DbSet<Movie> Movies { get; set; }

        public Context() : base("ACCv2.2")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ChallengeItem>()
                .HasRequired(m=>m.Challenge)
                .WithMany(m=>m.ChallengeItems)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<Challenge>()
                .HasRequired(m => m.User)
                .WithMany(m => m.Challenges)
                .WillCascadeOnDelete(true);


        }
    }
}

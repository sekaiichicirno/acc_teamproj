﻿using ACC.Core.Models;
using ACC.Core.Models.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ACC.Core
{
    public class QueryLogic
    {
        private const string appKey = "3c0c482d";
        private const string searchBase = "http://www.omdbapi.com/";

        public List<Movie> SearchMovies(string request) //add try catch in Controller? or in main
        {
            using (var client = new HttpClient())
            {
                var result = client.GetStringAsync(MakeQuery(request)).Result;
                return JsonConvert.DeserializeObject<MoviesByTitleResponse>(result).Movies; //null if no movies were found
            }
        }

        private string MakeQuery(string request)
        {
            return BuildUrl(searchBase, new Dictionary<string, string>
            {
                {"apikey",appKey},
                {"s", request}
            });
        }

        private string BuildUrl(string baseUrl, IDictionary<string, string> parameters)
        {
            var sb = new StringBuilder(baseUrl);
            if (parameters?.Count > 0)
            {
                sb.Append('?');
                foreach (var p in parameters)
                {
                    sb.Append(p.Key);
                    sb.Append("=");
                    sb.Append(WebUtility.UrlEncode(p.Value));
                    sb.Append('&');
                }
                sb.Remove(sb.Length - 1, 1);
            }
            return sb.ToString();
        }

    }
}

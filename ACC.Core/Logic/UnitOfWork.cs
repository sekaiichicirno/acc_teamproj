﻿using ACC.Core.Logic.Repository;
using ACC.Core.Models;
using System;

namespace ACC.Core.Logic
{
    public class UnitOfWork : IDisposable
    {
        private Context context = new Context();
        private readonly GenericRepository<Context, User> userRepository;
        private readonly GenericRepository<Context, Challenge> challengeRepository;

        public GenericRepository<Context, User> UserRepository
        {
            get{return userRepository ?? new GenericRepository<Context, User>(context);}
        }
        public GenericRepository<Context, Challenge> ChallengeRepository
        {
            get{return challengeRepository ?? new GenericRepository<Context, Challenge>(context);}
        }

        public void Save()
        {
            context.SaveChanges();
        }

        //implementing IDispose
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

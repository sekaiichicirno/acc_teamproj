﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC.Core.Logic.Repository
{
    public interface IRepository<T> where T : class
    {
        void Add(T entity);
        T GetAll(T entity);
        void Edit(T entity);
        void Delete(T entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;

namespace ACC.Core.Logic.Repository
{
    public class GenericRepository<C,TEntity> where C : DbContext where TEntity : class
    {
        internal C Context;
        internal DbSet<TEntity> DbSet;

        //"CRUD" HERE
        public GenericRepository(C context)
        {
            Context = context;
            DbSet = context.Set<TEntity>();
        }

        public virtual void Add(TEntity entity)
        {
            DbSet.Add(entity);
        }
        public virtual void Delete(TEntity entity)
        {
            if (Context.Entry(entity).State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            DbSet.Remove(entity);
        }
        public virtual void Edit(TEntity entity, Expression<Func<TEntity, object>> filter)
        {
            //DbSet.AddOrUpdate(filter,entity);
            DbSet.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }

        //get methods
        public virtual TEntity GetById(int id)
        {
            return DbSet.Find(id);
        }
        public virtual IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> filter = null, string includeProperties = "")
        {
            return GetQueryable(filter, includeProperties);
        }
        public virtual TEntity GetFirst(Expression<Func<TEntity, bool>> filter = null, string includeProperties = "")
        {
            return GetQueryable(filter, includeProperties).FirstOrDefault();
        }
        protected virtual IQueryable<TEntity> GetQueryable(Expression<Func<TEntity, bool>> filter = null, string includeProperties = "")
        {
            includeProperties = includeProperties ?? string.Empty;
            IQueryable<TEntity> query = DbSet;
            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            return query;
        }
    }
}

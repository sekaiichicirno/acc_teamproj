﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC.Core.Exceptions
{
    public class CustomException : Exception
    {
        public CustomException(int passwordLength) : base($"Your password is {passwordLength} symbols long! It should be from 8 to 64 symbols long!")
        {

        }
        public CustomException(string customMessage) : base(customMessage)
        {

        }
    }
}

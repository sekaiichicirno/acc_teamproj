﻿using ACC.Core.Logic;
using ACC.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ACC.Core.Exceptions;

namespace ACC.Core.Controllers
{
    public class Controller : IController
    {
        public User CurrentUser { get; set; }
        private QueryLogic queryLogic = new QueryLogic();
        //private UnitOfWork unitOfWork = new UnitOfWork(); //!!у него унаследован IDisposable!!

        public void UserRegister(string login, string password, string name)// тут специально выдаётся ошибка (ArgumentException), когда логин занят.
        {
            if (password.Length < 8 || password.Length > 64)
                throw new CustomException(password.Length);
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                if (unitOfWork.UserRepository.GetFirst(u => u.Login.Equals(login)) != null)
                    throw new CustomException("This login was taken.");
                User user = new User
                {
                    Login = login,
                    Password = Secure(password),
                    Name = name
                };
                unitOfWork.UserRepository.Add(user);
                unitOfWork.Save();
            }
        }

        public void UserLogin(string login, string password) //выдаёт ошибку, если логин и/или пароль неверные
        {
            string hashedPass = Secure(password);
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                User user = unitOfWork.UserRepository.GetFirst(u => u.Login.Equals(login) && u.Password.Equals(hashedPass));
                if (user != null)
                    CurrentUser = user;
                else
                {
                    unitOfWork.Dispose();
                    throw new CustomException("Data is incorrect!");
                }
            }
        }

        public void DeleteAccount()
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                unitOfWork.UserRepository.Delete(CurrentUser);
                unitOfWork.Save();
            }
            CurrentUser = null;
        }

        public void CreateChallenge(string title, int maxItems)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
            var chal = new Challenge
            {
                Title = title,
                IsCompleted = false,
                IsDropped = false,
                MaxItems = maxItems,
                User = unitOfWork.UserRepository.GetFirst(u => u.Login.Equals(CurrentUser.Login))
            };
            
                unitOfWork.ChallengeRepository.Add(chal);
                unitOfWork.Save();
            }
            if (CurrentUser.Challenges!=null)
            {
                CurrentUser.Challenges = null;
                GetChallenges();
            }
        }

        public List<Challenge> GetChallenges()
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                if (CurrentUser.Challenges == null)
                {
                    CurrentUser.Challenges = new List<Challenge>();
                    CurrentUser.Challenges = unitOfWork.ChallengeRepository.GetAll(c => c.User.Id == CurrentUser.Id,"User").ToList();
                }
                return CurrentUser.Challenges;
            }
        }

        public Challenge LoadChallenge(Challenge challenge)
        {
            int chl = CurrentUser.Challenges.FindIndex(c => c.Id == challenge.Id);
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                if (CurrentUser.Challenges[chl].ChallengeItems == null)
                {
                    CurrentUser.Challenges[chl] = unitOfWork.ChallengeRepository.GetFirst(c => c.Id.Equals(challenge.Id), "ChallengeItems");
                }
                if (CurrentUser.Challenges[chl].ChallengeItems == null)
                    CurrentUser.Challenges[chl].ChallengeItems = new List<ChallengeItem>();
                return CurrentUser.Challenges[chl];
            }
        }

        public void UpdateChallenge(Challenge challengeEdited)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Challenge challengeOld = unitOfWork.ChallengeRepository.GetById(challengeEdited.Id);
                challengeOld.ChallengeItems = challengeEdited.ChallengeItems;

                unitOfWork.Save();
            }
        }

        public void UpdateChallenges()
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                foreach (var challenge in CurrentUser.Challenges)
                    unitOfWork.ChallengeRepository.Edit(challenge,c=>c.Id);
                unitOfWork.Save();
            }
        }
        public void DeleteChallenge(Challenge challenge)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                var chal = LoadChallenge(challenge);
                CurrentUser.Challenges.Remove(chal);
                unitOfWork.ChallengeRepository.Delete(chal);
                unitOfWork.Save();
            }
        }
        public void CompleteChallenge(Challenge challenge)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                if (IsChallengeCompleted(challenge))
                {
                    challenge.IsCompleted = true;
                    challenge.IsDropped = false;
                    unitOfWork.ChallengeRepository.Edit(challenge, c=>c.Id);
                    unitOfWork.Save();
                    CurrentUser.Challenges = null;
                    GetChallenges();
                    LoadChallenge(challenge);
                }
                else
                {
                    throw new CustomException("Cannot complete challenge");
                }
            }
        }

        public List<Movie> SearchMovies(string movieTitle)
        {
            return queryLogic.SearchMovies(movieTitle);
        }

        //челлендж айтемы
        public void AddChallengeItem(Movie movie, Challenge challenge)
        {
            GetChallenges();
            if (challenge.ChallengeItems==null)
            {
                challenge.ChallengeItems = new List<ChallengeItem>();
            }
            var test = IsChallengeCompleted(challenge);
            if (IsChallengeCompleted(challenge))
                throw new CustomException("This challenge is already completed.");
            var newItem = new ChallengeItem
            {
                Movie = movie,
                DateAdded = DateTime.Now,
                IsCompleted = false,
            };
            challenge.ChallengeItems.Add(newItem);
            UpdateChallenge(challenge);
            //using (unitOfWork = new UnitOfWork())
            //{
            //    unitOfWork.ChallengeRepository.Edit(challenge,c=>c.Id);
            //    unitOfWork.Save();
            //}
        }
        public void DeleteChallengeItem(ChallengeItem challengeItem)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                var challengeFound = unitOfWork.ChallengeRepository.GetFirst(c => c.Id == challengeItem.Challenge.Id,"ChallengeItems");
                challengeFound.ChallengeItems.Remove(challengeItem);
                //unitOfWork.ChallengeRepository.Edit(challengeFound, c=>c.Id);
                unitOfWork.Save();
            }
        }

        //вспомогательные функции
        public bool IsChallengeCompleted(Challenge challenge)
        {
            if (challenge.IsCompleted)
                return true;
            if (challenge.ChallengeItems == null)
                challenge.ChallengeItems = new List<ChallengeItem>();
            if (!(challenge.ChallengeItems.Count == challenge.MaxItems))
                return false;
            else
            {
                foreach (var item in challenge.ChallengeItems)
                   if (!item.IsCompleted)
                        return false;
                return true;
            }
        }
        private string Secure(string password)
        {
            var md5 = MD5.Create();
            var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(password));
            return Convert.ToBase64String(hash);
        }
    }
}

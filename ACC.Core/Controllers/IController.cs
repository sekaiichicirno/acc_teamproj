﻿using ACC.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC.Core.Controllers
{
    public interface IController
    {
        User CurrentUser { get; set; }

        //везде,где в комм. CustomException, можно получить информацию об ошибке, используя Message, и вевести в MessageBox. в сообщении будет сказано, в чём проблема.
        void UserRegister(string login, string password, string name);//регистрация. CustomException
        void UserLogin(string login, string password);//логин. CustomException
        void DeleteAccount();//удалить акк текущего пользователя.

        List<Challenge> GetChallenges(); //получить челленджи пользователя
        void CreateChallenge(string title, int maxItems);//создать новый челлендж, используя макс. к-во элементов и название
        Challenge LoadChallenge(Challenge challenge);//получить какой то конкретный из челленджей пользователя
        //void EditChallenge(Challenge challenge);
        void DeleteChallenge(Challenge challenge);//удалить челлендж
        void UpdateChallenges();//обновить челленджи текущего пользователя. оно может не робить)) не используйте его плз
        void CompleteChallenge (Challenge challenge);//завершить челлендж. CustomException, если челленддж не может быть завершен

        void DeleteChallengeItem(ChallengeItem challengeItem);
        void AddChallengeItem(Movie movie, Challenge challenge); //добавить фильм в челлендж. CustomException, если челлендж уже завершен
        //void EditChallengeItem();

        List<Movie> SearchMovies(string movieTitle);//если что, лист будет null, если фильмы не найдены. +нужно добавить try-catch, т.к. мб ошибка из-за интернета

    }
}

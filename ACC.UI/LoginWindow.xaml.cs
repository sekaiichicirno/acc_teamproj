﻿using ACC.Core.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ACC.UI
{
    /// <summary>
    /// Логика взаимодействия для LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private IController _controller;
        public LoginWindow()
        {
            InitializeComponent();
            _controller = Globals.MainController;
        }
        private void ButtonLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _controller.CurrentUser = null;
                _controller.UserLogin(textBoxLogin.Text, passwordBox.Password);
            }
            catch (Exception)
            {
                MessageBox.Show("Please check the correctness of entered data (login or password)");
            }
            if (_controller.CurrentUser != null)
            {
                var wndM = new MainWindow();
                wndM.Show();
                Close();
            }
        }

        private void ButtonRegister_Click(object sender, RoutedEventArgs e)
        {
            var registerWindow = new RegisterWindow();
            registerWindow.RegistrationFinished += RegisterWindow_RegistrationFinished;
            Hide();
            registerWindow.ShowDialog();
            Show();
       
        }

        private void RegisterWindow_RegistrationFinished()
        {
            Show();
        }

    }
}

﻿using ACC.Core;
using ACC.Core.Controllers;
using ACC.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace ACC.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IController _controller;
        public MainWindow()
        {
            InitializeComponent();
            _controller = Globals.MainController;
            var listChallenges = _controller.GetChallenges();
            listBoxChallenges.ItemsSource = listChallenges;
        }
        private void BtnGoClick(object sender, RoutedEventArgs e)
        {
            string searchText = textBoxSearch.Text;
            var listMovies = _controller.SearchMovies(searchText).Take(10).ToList();
            listBoxMovies.ItemsSource = listMovies;
        }

        private void BtnAddClick(object sender, RoutedEventArgs e)
        {
            object objM = listBoxMovies.SelectedItem;
            if (objM == null)
            {
                MessageBox.Show("Please, select film!");
                return;
            }
            Globals.SelectedMovie = objM as Movie;
            object objC = listBoxChallenges.SelectedItem;
            if (objC == null)
            {
                MessageBox.Show("Please, select challenge!");
                return;
            }
            var challenge = objC as Challenge;
            if (challenge.ChallengeItems == null)
            {
                challenge.ChallengeItems = new List<ChallengeItem>();
            }
            int count = challenge.ChallengeItems.Count;
            if (count == 10)
            {
                MessageBox.Show("There are already 10 films! Please finish the active challenge.");
            }
            else
            {
                // challenge.ChallengeItems.Add(new ChallengeItem() { Movie = movie });
                _controller.AddChallengeItem(Globals.SelectedMovie, challenge);
                MessageBox.Show("was added, count = "+ challenge.ChallengeItems.Count.ToString());
            }
        }

        private void BtnEditChallenges(object sender, RoutedEventArgs e)
        {
            listBoxChallenges.ItemsSource = null;
            var wnd = new ChallengesListWindow();
            wnd.ShowDialog();
            listBoxChallenges.ItemsSource = _controller.GetChallenges();
        }

        private void btnAccount_Click(object sender, RoutedEventArgs e)
        {
            _controller.DeleteAccount();
            Close();
        }
    }
}

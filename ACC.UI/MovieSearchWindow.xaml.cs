﻿using ACC.Core.Controllers;
using ACC.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ACC.UI
{
    /// <summary>
    /// Логика взаимодействия для MovieSearchWindow.xaml
    /// </summary>
    public partial class MovieSearchWindow : Window
    {
        private IController _controller;

        public MovieSearchWindow()
        {
            InitializeComponent();
            _controller = Globals.MainController;
        }

        private void BtnGoClick(object sender, RoutedEventArgs e)
        {
            string searchText = textBoxSearch.Text;
            var listMovies = _controller.SearchMovies(searchText).Take(10).ToList();
            listBoxMovies.ItemsSource = listMovies;
        }

        private void BtnAddClick(object sender, RoutedEventArgs e)
        {
            object objM = listBoxMovies.SelectedItem;
            if (objM == null)
            {
                MessageBox.Show("Please, select film!");
            }
            Globals.SelectedMovie = objM as Movie;
            Close();
        }
    }
}

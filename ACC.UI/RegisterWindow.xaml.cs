﻿using ACC.Core.Controllers;
using ACC.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ACC.UI
{
    /// <summary>
    /// Логика взаимодействия для RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : Window
    {
        private IController _controller;
        public event Action RegistrationFinished;
        public RegisterWindow()
        {
            InitializeComponent();
            _controller = Globals.MainController;
        }

        private void Button_Click_Register(object sender, RoutedEventArgs e)
        {
            try
            {
                _controller.UserRegister(textBoxLogin.Text, passwordBox.Password, textBoxName.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("This login was taken.");
            }
            Close();
        }
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            RegistrationFinished?.Invoke();
            Close();
        }
    }
}

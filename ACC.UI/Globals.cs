﻿using ACC.Core.Controllers;
using ACC.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACC.UI
{
    public class Globals
    {
        public static IController MainController = new Controller();
        public static Movie SelectedMovie { get; set; }
    }
}

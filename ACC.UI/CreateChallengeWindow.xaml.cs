﻿using ACC.Core.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ACC.UI
{
    /// <summary>
    /// Логика взаимодействия для CreateChallengeWindow.xaml
    /// </summary>
    public partial class CreateChallengeWindow : Window
    {
        private IController _controller;
        public CreateChallengeWindow()
        { 
            InitializeComponent();
            _controller = Globals.MainController;
        }

        private void CreateBtn_Click(object sender, RoutedEventArgs e)
        {
            int.TryParse(textBoxMaxItems.Text, out int max);
            _controller.CreateChallenge(textBoxName.Text, max);
            Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}

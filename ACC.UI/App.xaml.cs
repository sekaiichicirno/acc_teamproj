﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace ACC.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            var controller = Globals.MainController;
            var wnd = new LoginWindow();
            wnd.ShowDialog();
            if (controller.CurrentUser != null)
            {
                var wndM = new MainWindow();
                wndM.Show();
            }
        }
    }
}

﻿using ACC.Core.Controllers;
using ACC.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ACC.UI
{
    /// <summary>
    /// Логика взаимодействия для ChallengesListWindow.xaml
    /// </summary>
    public partial class ChallengesListWindow : Window
    {
        private IController _controller;
        private Challenge _currentChallenge;

        public ChallengesListWindow()
        {
            InitializeComponent();
            _controller = Globals.MainController;
            this.Loaded += ChallengesListWindow_Loaded;
        }

        private void ChallengesListWindow_Loaded(object sender, RoutedEventArgs e)
        {
            SetChallenges();
        }

        private void SetChallenges()
        {
            listBoxChallenges.ItemsSource = null;
            var listChallenges = _controller.GetChallenges();
            listBoxChallenges.ItemsSource = listChallenges;
        }

        private void SelectionChallenge(object sender, SelectionChangedEventArgs e)
        {
            var obj = listBoxChallenges.SelectedItem;
            if (obj ==null)
            {
                _currentChallenge = null;
            }
            else
            {
                _currentChallenge = obj as Challenge;
                listBoxMovies.ItemsSource = _currentChallenge.ChallengeItems;
            }
        }

        private void BtnAddClick(object sender, RoutedEventArgs e)
        {
            var wnd = new CreateChallengeWindow();
            wnd.ShowDialog();
            //
            // _controller.CreateChallenge(,);
            SetChallenges();
            _controller.UpdateChallenges();
        }

        private void BtnRemoveClick(object sender, RoutedEventArgs e)
        {
            object objC = listBoxChallenges.SelectedItem;
            if (objC == null)
            {
                MessageBox.Show("Please, select challenge!");
            }
            var challenge = objC as Challenge;
            _controller.DeleteChallenge(challenge);
            listBoxChallenges.ItemsSource = null;
            SetChallenges();
            _controller.UpdateChallenges();
        }

        private void BtnFinishClick(object sender, RoutedEventArgs e)
        {
            object objC = listBoxChallenges.SelectedItem;
            if (objC == null)
            {
                MessageBox.Show("Please, select challenge!");
            }
            var challenge = objC as Challenge;
            _controller.CompleteChallenge(challenge);
            listBoxChallenges.ItemsSource = null;
            SetChallenges();
            _controller.UpdateChallenges();
        }

        private void btnAddMovie_Click(object sender, RoutedEventArgs e)
        {
            if (_currentChallenge == null)
            {
                MessageBox.Show("Please, select challege before!");
                return;
            }
            Globals.SelectedMovie = null;
            var wnd = new MovieSearchWindow();
            wnd.ShowDialog();
            if (Globals.SelectedMovie == null)
            {
                MessageBox.Show("Please, search and select new movie!");
                return;
            } else
            {
                var item = new ChallengeItem();
                item.Challenge = _currentChallenge;
                item.Movie = Globals.SelectedMovie;
                if (_currentChallenge.ChallengeItems == null)
                    _currentChallenge.ChallengeItems = new List<ChallengeItem>();

                //1) Add
                //_currentChallenge.ChallengeItems.Add(item);

                // 2) Add
                _controller.AddChallengeItem(Globals.SelectedMovie, _currentChallenge);

                listBoxMovies.ItemsSource = _currentChallenge.ChallengeItems;
                MessageBox.Show("Done!");
            }
        }

        private void btnRemoveMovie_Click(object sender, RoutedEventArgs e)
        {
            object objM = listBoxMovies.SelectedItem;
            if (objM == null)
            {
                MessageBox.Show("Please, select movie!");
            }
            var movie = objM as ChallengeItem;
            _currentChallenge.ChallengeItems.Remove(movie);
            listBoxMovies.ItemsSource = null;
            listBoxChallenges.ItemsSource = _currentChallenge.ChallengeItems;
            _controller.UpdateChallenges();

        }
    }
}

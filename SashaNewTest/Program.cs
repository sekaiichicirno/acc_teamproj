﻿using ACC.Core.Controllers;
using System;
using System.Linq;
using ACC.Core.Exceptions;

namespace SashaNewTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("registration");
            Console.WriteLine("login: "); string login = Console.ReadLine();
            Console.WriteLine("password: "); string password = Console.ReadLine();
            Console.WriteLine("name: "); string name = Console.ReadLine();

            Controller controller = new Controller();
            try
            {
                controller.UserRegister(login, password, name);
                Console.WriteLine("register successful. trying to login");
                controller.UserLogin(login, password);
                Console.WriteLine("ok");
            }
            catch(CustomException pl)
            {
                string s = pl.Message;
                Console.WriteLine(s);
            }
            //Console.WriteLine("make a challenge: ");
            //Console.WriteLine("title:"); string chTitle = Console.ReadLine();
            //Console.WriteLine("# of items: "); int chItems = int.Parse(Console.ReadLine());
            //controller.CreateChallenge(chTitle, chItems);


            Console.WriteLine("testing challenge creation:");
            Console.WriteLine("challenge title:"); string chTitle = Console.ReadLine();
            Console.WriteLine("challenge items:"); int chMaxItems = int.Parse(Console.ReadLine());
            controller.CreateChallenge(chTitle,chMaxItems);
            var chCreated = controller.GetChallenges()[0];
            Console.WriteLine($"Id: {chCreated.Id} Title:{chCreated.Title} Completion:{chCreated.IsCompleted} Drop:{chCreated.IsDropped} Items:{chCreated.MaxItems}");
            Console.WriteLine("success.trying to load");
            controller.LoadChallenge(chCreated);
            Console.WriteLine("loaded new challenge successfully");
            Console.WriteLine("adding an item to movies");
            string movieTitle;
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("movie title: ");
                movieTitle = Console.ReadLine();
                controller.AddChallengeItem(controller.SearchMovies(movieTitle)[0], controller.CurrentUser.Challenges.FirstOrDefault(c=>c.Title.Equals(chCreated.Title)));
            }
            Console.WriteLine("added 10 items successfully");
            Console.WriteLine();
            foreach (var item in chCreated.ChallengeItems)
            {
                Console.WriteLine($"Id: {item.Id} Completed: {item.IsCompleted} Score: {item.Score} Date added: {item.DateAdded} Movie: {item.Movie.Title}");
            }
            Console.WriteLine();
            Console.WriteLine("testing deletion");
            Console.WriteLine("trying to delete 0th element in a challenge");
            controller.DeleteChallengeItem(chCreated.ChallengeItems[0]);
            Console.WriteLine("successfully deleted");
            try
            {
                Console.WriteLine($"{controller.CurrentUser.Name}");
                controller.CurrentUser.Challenges = null;
                //controller.UserRegister(login, password, name);
                //controller.UserLogin(login, password);
                Console.WriteLine("Success");
                var challenges = controller.GetChallenges();
                int l = challenges.Count;
                int i = 0;
                while (i < l){
                    if (challenges[i].ChallengeItems == null)
                        controller.LoadChallenge(challenges[i]);
                    i += 1;
                }
                    
                foreach (var item in challenges)
                {
                    try
                    {
                        controller.CompleteChallenge(item);
                        Console.WriteLine($"Challenge {item.Title} successfully completed");
                    }
                    catch (CustomException ce)
                    {
                        string u = ce.Message;
                        Console.WriteLine($"Challenge {item.Title} was not completed");
                    }
                }
                Console.WriteLine("<===END===>");
                Console.WriteLine();
                Console.WriteLine();
                //Console.WriteLine("create a challenge:");
                //Console.WriteLine("challenge title: "); string title = Console.ReadLine();
                //Console.WriteLine("number of movies: "); int num = int.Parse(Console.ReadLine());
                //controller.CreateChallenge(title, num);
                //var chacha =  controller.CurrentUser.Challenges.FirstOrDefault(c => c.Title.Equals("PHP"));
                //controller.DeleteChallenge(chacha);



                //var deleteMe = controller.CurrentUser.Challenges[0].ChallengeItems.FirstOrDefault(c => c.Movie.Title == "SHREK");
                //controller.DeleteChallengeItem(deleteMe);
                //Console.WriteLine($"{deleteMe.Movie.Title} item deleted");



                //var lecic = controller.SearchMovies("back to the future");
                //try
                //{
                //    controller.AddChallengeItem(lecic[0], controller.GetChallenges()[1]);
                //}
                //catch (ArgumentException)
                //{
                //    Console.WriteLine("This challenge was comleted");
                //}
                Console.WriteLine();
            }
            catch (ArgumentException)
            {

                Console.WriteLine("data is incorrect");
            }

            Console.ReadLine();
        }
    }
}
